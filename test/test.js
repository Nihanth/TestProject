var request = require('supertest');
var app = express();
module.exports =app;
 
describe('GET /', function() {
  it('respond with hello world', function(done) {
    request(app).get('/').expect('hello world', done);
  });
});
